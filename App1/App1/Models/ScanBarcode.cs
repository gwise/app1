﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace App1.Models
{
    public class ScanBarcode : BindableObject
    {
        private int _qty;

        public string Format { get; set; }

        public string Result { get; set; }

        public int Qty
        {
            get { return this._qty; }
            set
            {
                if (this._qty == value) return;
                this._qty = value;
                OnPropertyChanged("Qty");
            }
        }
    }
}
