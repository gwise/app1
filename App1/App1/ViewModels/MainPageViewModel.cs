﻿using App1.Models;
using DevExpress.Mobile.DataGrid;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace App1.ViewModels
{
    public class MainPageViewModel : ViewModelBase
    {
        private ObservableCollection<ScanBarcode> _scanBarcode = new ObservableCollection<ScanBarcode>();
        private IList<ScanBarcode> _scanBarcode2 = new List<ScanBarcode>();

        public ICommand TapCommand { get; }
        public ICommand DoubleTapCommand { get; }

        public MainPageViewModel()
        {
            TapCommand = new Command(OnTapped);
            DoubleTapCommand = new Command(OnDoubleTapped);

            for (int i = 0; i < 100; i++)
            {
                ScanBarcodeList.Add(new ScanBarcode
                {
                    Format = "Code128",
                    Result = "12345678" + i.ToString(),
                    Qty = i + 1
                });
            }
        }

        public void OnTapped(object s)
        {
            Debug.WriteLine("parameter: " + s.ToString());
        }

        public void OnDoubleTapped(object s)
        {
            Debug.WriteLine("parameter: " + s.ToString());
        }

        public void Save()
        {
            //_scanBarcode2.Clear();
            _scanBarcode2 = new List<ScanBarcode>();
            foreach (var item in ScanBarcodeList)
            {
                Debug.WriteLine(item.Format);
                Debug.WriteLine(item.Result);
                Debug.WriteLine(item.Qty);

                _scanBarcode2.Add(new ScanBarcode
                {
                    Format = item.Format,
                    Result = item.Result,
                    Qty = item.Qty
                });
            }
        }

        public void Search()
        {
            //화면에서 수정했던 cell을 다시 수정 할 수 없다.
            //ScanBarcodeList = null;
            //ScanBarcodeList = new ObservableCollection<ScanBarcode>(_scanBarcode2);

            //이 방법은 문제 없이 cell을 수정 할 수 있다.
            ScanBarcodeList.Clear();
            foreach (var item in _scanBarcode2)
            {
                ScanBarcodeList.Add(new ScanBarcode
                {
                    Format = item.Format,
                    Result = item.Result,
                    Qty = item.Qty
                });
            }
        }

        int selectedCount = 0;
        int RowHandle = -1;
        public void GridDoubleTapped(RowTapEventArgs e)
        {
            //ToDo
            Debug.WriteLine(e.RowHandle);
            Debug.WriteLine(e.FieldName);
            Debug.WriteLine(e.ToString());
            //Debug.WriteLine(((GridControl)sender).GetCellValue(e.RowHandle, e.FieldName).ToString());
            //Debug.WriteLine(((GridControl)sender).GetCellDisplayText(e.RowHandle, e.FieldName).ToString());

            
            if (RowHandle == e.RowHandle)
            {
                selectedCount += 1;
                if (selectedCount == 2)
                {
                    Debug.WriteLine("--------------------");
                    Debug.WriteLine("Grid Double Tapped");
                    Debug.WriteLine("--------------------");
                    selectedCount = 0;
                }
            }
            else
            {
                this.RowHandle = e.RowHandle;
                selectedCount = 1;
            }
        }

        public ObservableCollection<ScanBarcode> ScanBarcodeList
        {
            get { return _scanBarcode; }

            set
            {
                if (_scanBarcode == value) return;
                _scanBarcode = value;
                OnPropertyChanged("ScanBarcodeList");
            }
        }

    }
}
